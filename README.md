# popl-project-group-1

This project consists of 3 topics:

* CYK Algorithm
* Breadth First Traversal
* Functional Programming

For each of these topics we have established one/more transition systems, in order to enable the user to understand the topic clearly.
To access these systems, clone the repository:

```bash
git clone https://gitlab.com/saujasv/popl-project-group-1.git
```

and open `ui/index.html` in your browser.

Use `specification.pdf` for more information on how each system is defined.
The demo video is available [here](https://youtu.be/26O6iGyAbp4).
