module Interpreter exposing (..)
import Html exposing (b)

import Debug

-- The following are the list of term nodes in the term graph. We encode a term as a procedure by having no children.
-- A term of the form `TermAdd Nothing Nothing` indicates that it is a procedure.
-- For simplicity, we don't allow map, reduce and filter to be used as arguments to other functions.
type Term
    = TermNumeral Int
    | TermMap Term Term
    | TermReduce Term Term Term
    | TermFilter Term Term
    | TermCompose Term Term
    | TermMultiply (Maybe Term) (Maybe Term)
    | TermAdd (Maybe Term) (Maybe Term)
    | TermSub (Maybe Term) (Maybe Term)
    | TermSquare (Maybe Term)
    | TermCube (Maybe Term)
    | TermAdd1 (Maybe Term)
    | TermSub1 (Maybe Term)
    | TermIdentity (Maybe Term)
    | TermNot (Maybe Term)
    | TermIsZero (Maybe Term)
    | TermIsPositive (Maybe Term)
    | TermIsNegative (Maybe Term)
    | TermList (List Term)
    | TermLiteralList (List Int) -- same as TermList except that it can only store literal values instead of subtrees
    | TermProcedure (List Term)

getTermName : Term -> String
getTermName t =
    case t of
        TermNumeral _ -> "TermNumeral"
        TermMap _ _ -> "TermMap"
        TermReduce _ _ _ -> "TermReduce"
        TermFilter _ _ -> "TermFilter"
        TermCompose _ _ -> "TermCompose"
        TermMultiply _ _ -> "TermMultiply"
        TermAdd _ _ -> "TermAdd"
        TermSub _ _ -> "TermSub"
        TermSquare _ -> "TermSquare"
        TermCube _ -> "TermCube"
        TermAdd1 _ -> "TermAdd1"
        TermSub1 _ -> "TermSub1"
        TermIdentity _ -> "TermIdentity"
        TermNot _ -> "TermNot"
        TermIsZero _ -> "TermIsZero"
        TermIsPositive _ -> "TermIsPositive"
        TermIsNegative _ -> "TermIsNegative"
        TermList _ -> "TermList"
        TermLiteralList _ -> "TermLiteralList"
        TermProcedure _ -> "TermProcedure"

-- UTILS

getNumber : Term -> Maybe Int
getNumber t =
    case t of
        TermNumeral n -> Just n
        _ -> Nothing

numeralNot : Int -> Int
numeralNot t = if t == 0 then 1 else 0

-- ERRORS

type RewriteRuleError =
    RRERR_REDEX_MATCH_FAIL_TERM String |
    RRERR_REDEX_MATCH_FAIL_EVAL_CHILD String |
    RRERR_UNEXPECTED_PROCEDURE String |
    RRERR_PROC_ARGMISMATCH String |
    RRERR_OTHER String

type Result value
  = OK value
  | Error RewriteRuleError

getErrorString e =
    case e of
        RRERR_REDEX_MATCH_FAIL_TERM s -> "RRERR_REDEX_MATCH_FAIL_TERM >> " ++ s
        RRERR_REDEX_MATCH_FAIL_EVAL_CHILD s -> "RRERR_REDEX_MATCH_FAIL_EVAL_CHILD >> " ++ s
        RRERR_UNEXPECTED_PROCEDURE s -> "RRERR_UNEXPECTED_PROCEDURE >> " ++ s
        RRERR_PROC_ARGMISMATCH s -> "RRERR_PROC_ARGMISMATCH >> " ++ s
        RRERR_OTHER s -> "RRERR_OTHER >> " ++ s

-- FUNCTIONS
-- the evaluate functions here apply a procedure on terms; for example, applying a procedure like 'TermSquare Nothing' on 'TermNumeral 5'

evaluate_proc1 : Term -> Term -> Result Term
evaluate_proc1 proc arg =
    let
        evaluate_proc1_base : Term -> Term -> Result Term
        evaluate_proc1_base f arg1 =
            case f of
                TermSquare Nothing -> rewriteRuleSQUARE (TermSquare (Just arg1))
                TermCube Nothing -> rewriteRuleCUBE (TermCube (Just arg1))
                TermAdd1 Nothing -> rewriteRuleADD1 (TermAdd1 (Just arg1))
                TermSub1 Nothing -> rewriteRuleSUB1 (TermSub1 (Just arg1))
                TermIdentity Nothing -> rewriteRuleIDENTITY (TermIdentity (Just arg1))
                TermNot Nothing -> rewriteRuleNOT (TermNot (Just arg1))
                TermIsZero Nothing -> rewriteRuleISZERO (TermIsZero (Just arg1))
                TermIsPositive Nothing -> rewriteRuleISPOSITIVE (TermIsPositive (Just arg1))
                TermIsNegative Nothing -> rewriteRuleISNEGATIVE (TermIsNegative (Just arg1))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "not a procedure with single parameter")

        reduce2 : (List Term) -> Term -> Result Term
        reduce2 list init =
            case list of
                [] -> Error (RRERR_OTHER "empty procedure")
                x :: [] -> (evaluate_proc1_base x init)
                x :: xs ->
                    case (reduce2 xs init) of
                        OK t -> evaluate_proc1_base x t
                        Error e -> Error e
    in
    case proc of
        TermProcedure lst -> reduce2 lst arg
        _ -> Error (RRERR_OTHER "not a procedure")

evaluate_proc2 : Term -> Term -> Term -> Result  Term
evaluate_proc2 proc arg1_ arg2_ =
    let
        evaluate_proc2_base : Term -> Term -> Term -> Result Term
        evaluate_proc2_base item arg1 arg2 =
            case item of
                TermMultiply Nothing Nothing -> rewriteRuleMULT (TermMultiply (Just arg1) (Just arg2))
                TermAdd Nothing Nothing -> rewriteRuleADD (TermAdd (Just arg1) (Just arg2))
                TermSub Nothing Nothing -> rewriteRuleSUB (TermSub (Just arg1) (Just arg2))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "not a procedure with two parameters")
    in
    case proc of
        TermProcedure lst ->
            case lst of
                [] -> Error (RRERR_OTHER "empty procedure")
                p :: [] -> evaluate_proc2_base p arg1_ arg2_
                _ -> Error (RRERR_OTHER "invalid composition of multiarg functions")
        _ -> Error (RRERR_OTHER "not a procedure")

-- REWRITE RULES

rewriteRuleMAP : Term -> Result Term
rewriteRuleMAP t =
    let
        doMAP : Term -> Term -> Result Term
        doMAP f_ lst_ =
            case (f_, lst_) of
                (TermProcedure _, TermLiteralList lst) ->
                    let
                        wrappedf : Int -> Int
                        wrappedf item =
                            case (evaluate_proc1 f_ (TermNumeral item)) of
                                OK (TermNumeral n) -> n
                                Error e -> Debug.todo (getErrorString e)
                                OK errorterm -> Debug.todo ("expected a numeral for map but found " ++ (getTermName errorterm))
                    in
                    OK (TermLiteralList (List.map wrappedf lst))
                _  -> Error (RRERR_PROC_ARGMISMATCH "arguments mismatch")
    in
    case t of
        TermMap f lst -> doMAP f lst
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "expected map term")
    
rewriteRuleREDUCE : Term -> Result Term
rewriteRuleREDUCE t =
    let
        doREDUCE : Term -> Term -> Term -> Result Term
        doREDUCE f_ init_ lst_ =
            case (f_, init_, lst_) of
                (TermProcedure f, TermNumeral init, TermLiteralList lst) ->
                    let
                        wrappedf : Int -> Int -> Int
                        wrappedf item acc =
                            case (evaluate_proc2 f_ (TermNumeral item) (TermNumeral acc)) of
                                OK (TermNumeral n) -> n
                                _ -> 0 -- TODO
                    in
                    OK (TermNumeral (List.foldl wrappedf init lst))
                _  -> Error (RRERR_PROC_ARGMISMATCH "arguments mismatch")
    in
    case t of
        TermReduce f init lst -> doREDUCE f init lst
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "expected reduce term")

rewriteRuleFILTER : Term -> Result Term
rewriteRuleFILTER t =
    let 
        doFILTER : Term -> Term -> Result Term
        doFILTER f_ lst_ =
            case (f_, lst_) of
                (TermProcedure f, TermLiteralList lst) ->
                    let
                        wrappedf : Int -> Bool
                        wrappedf item =
                            case (evaluate_proc1 f_ (TermNumeral  item)) of
                                OK (TermNumeral n) -> n == 1
                                _ -> False -- TODO
                    in
                    OK (TermLiteralList (List.filter wrappedf lst))
                _  -> Error (RRERR_PROC_ARGMISMATCH "arguments mismatch")
    in
    case t of
        TermFilter f lst -> doFILTER f lst
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "expected filter term")

rewriteRuleCOMPOSE : Term -> Result Term
rewriteRuleCOMPOSE t =
    case t of
        TermCompose (TermProcedure a) (TermProcedure b) -> OK (TermProcedure (List.append a b))
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required compose")

rewriteRuleMKPROC : Term -> Result Term
rewriteRuleMKPROC t =
    case t of
        TermMultiply Nothing Nothing -> OK (TermProcedure [t])
        TermAdd Nothing Nothing -> OK (TermProcedure [t])
        TermSub Nothing Nothing -> OK (TermProcedure [t])
        TermSquare Nothing -> OK (TermProcedure [t])
        TermCube Nothing -> OK (TermProcedure [t])
        TermAdd1 Nothing -> OK (TermProcedure [t])
        TermSub1 Nothing -> OK (TermProcedure [t])
        TermIdentity Nothing -> OK (TermProcedure [t])
        TermNot Nothing -> OK (TermProcedure [t])
        TermIsZero Nothing -> OK (TermProcedure [t])
        TermIsPositive Nothing -> OK (TermProcedure [t])
        TermIsNegative Nothing -> OK (TermProcedure [t])
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required primitive procedure")

rewriteRuleMULT : Term -> Result Term
rewriteRuleMULT t =
    case t of
        TermMultiply (Just (TermNumeral a)) (Just (TermNumeral b)) -> OK (TermNumeral (a * b))
        TermMultiply Nothing Nothing -> Error (RRERR_UNEXPECTED_PROCEDURE "expected literals; got procedure")
        TermMultiply _ _ -> Error (RRERR_REDEX_MATCH_FAIL_EVAL_CHILD "children must be literals")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermMultiply")

rewriteRuleADD : Term -> Result Term
rewriteRuleADD t =
    case t of
        TermAdd (Just (TermNumeral a)) (Just (TermNumeral b)) -> OK (TermNumeral (a + b))
        TermAdd Nothing Nothing -> Error (RRERR_UNEXPECTED_PROCEDURE "expected literals; got procedure")
        TermAdd _ _ -> Error (RRERR_REDEX_MATCH_FAIL_EVAL_CHILD "children must be literals")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermAdd")

rewriteRuleSUB : Term -> Result Term
rewriteRuleSUB t =
    case t of
        TermSub (Just (TermNumeral a)) (Just (TermNumeral b)) -> OK (TermNumeral (a - b))
        TermSub Nothing Nothing -> Error (RRERR_UNEXPECTED_PROCEDURE "expected literals; got procedure")
        TermSub _ _ -> Error (RRERR_REDEX_MATCH_FAIL_EVAL_CHILD "children must be literals")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermSub")
        
rewriteRuleSQUARE : Term -> Result Term
rewriteRuleSQUARE t =
    case t of
        TermSquare st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> OK (TermNumeral (a * a))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermSquare")
    

rewriteRuleCUBE : Term -> Result Term
rewriteRuleCUBE t =
    case t of
        TermCube st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> OK (TermNumeral (a * a * a))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermCube")
    
rewriteRuleADD1 : Term -> Result Term
rewriteRuleADD1 t =
    case t of
        TermAdd1 st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> OK (TermNumeral (a + 1))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermAdd1")
    
rewriteRuleSUB1 : Term -> Result Term
rewriteRuleSUB1 t =
    case t of
        TermSub1 st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> OK (TermNumeral (a - 1))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermSub1")
    
rewriteRuleIDENTITY : Term -> Result Term
rewriteRuleIDENTITY t =
    case t of
        TermIdentity st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just stv -> OK stv
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermIdentity")

rewriteRuleNOT : Term -> Result Term
rewriteRuleNOT t =
    case t of
        TermNot st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> OK (TermNumeral (numeralNot a))
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermNot")
    
rewriteRuleISZERO : Term -> Result Term
rewriteRuleISZERO t =
    case t of
        TermIsZero st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> if a == 0 then OK (TermNumeral 1) else OK (TermNumeral 0)
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermIsZero")

rewriteRuleISNEGATIVE : Term -> Result Term
rewriteRuleISNEGATIVE t =
    case t of
        TermIsNegative st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> if a < 0 then OK (TermNumeral 1) else OK (TermNumeral 0)
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermIsZero")

rewriteRuleISPOSITIVE : Term -> Result Term
rewriteRuleISPOSITIVE t =
    case t of
        TermIsPositive st ->
            case st of
                Nothing -> Error (RRERR_OTHER "no args provided")
                Just (TermNumeral a) -> if a > 0 then OK (TermNumeral 1) else OK (TermNumeral 0)
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required numeral")
        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required TermIsZero")

rewriteRuleEVALLIST : Term -> Result Term
rewriteRuleEVALLIST t =
    let
        isNumeral item =
            case item of
                TermNumeral n -> True
                _ -> False
        get item =
            case item of
                TermNumeral n -> n
                _ -> 0 -- impossible
    in
        case t of
            TermList lst ->
                if (List.all isNumeral lst) then
                    OK (TermLiteralList (List.map get lst))
                else
                    Error (RRERR_REDEX_MATCH_FAIL_EVAL_CHILD "children not literals")
            _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "required list")

-- EVALUATION

-- automatically applies the rewrite rules and evaluates the term graph
evaluateGraph : Term -> Result Term
evaluateGraph t =
    case t of
        -- base cases
        TermNumeral _ -> OK t -- singleton data
        TermLiteralList _ -> OK t -- list data
        TermProcedure _ -> OK t -- code data

        -- non-base cases
        TermMap f lst ->
            case (evaluateGraph f, evaluateGraph lst) of
                (OK t1, OK t2) -> rewriteRuleMAP (TermMap t1 t2)
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermReduce f init lst ->
            case (evaluateGraph f, evaluateGraph init, evaluateGraph lst) of
                (OK t1, OK t2, OK t3) -> rewriteRuleREDUCE (TermReduce t1 t2 t3)
                (Error e, _, _) -> Error e
                (_, Error e, _) -> Error e
                (_, _, Error e) -> Error e
        TermFilter f lst ->
            case (evaluateGraph f, evaluateGraph lst) of
                (OK t1, OK t2) -> rewriteRuleFILTER (TermFilter t1 t2)
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermCompose f1 f2 ->
            case (evaluateGraph f1, evaluateGraph f2) of
                (OK t1, OK t2) -> rewriteRuleCOMPOSE (TermCompose t1 t2)
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermMultiply Nothing Nothing -> rewriteRuleMKPROC(t)
        TermMultiply (Just lhs) (Just rhs) ->
            case (evaluateGraph lhs, evaluateGraph rhs) of
                (OK t1, OK t2) -> rewriteRuleMULT (TermMultiply (Just t1) (Just t2))
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermAdd Nothing Nothing -> rewriteRuleMKPROC(t)
        TermAdd (Just lhs) (Just rhs) ->
            case (evaluateGraph lhs, evaluateGraph rhs) of
                (OK t1, OK t2) -> rewriteRuleADD (TermAdd (Just t1) (Just t2))
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermSub Nothing Nothing -> rewriteRuleMKPROC(t)
        TermSub (Just lhs) (Just rhs) ->
            case (evaluateGraph lhs, evaluateGraph rhs) of
                (OK t1, OK t2) -> rewriteRuleMULT (TermSub (Just t1) (Just t2))
                (Error e, _) -> Error e
                (_, Error e) -> Error e
        TermSquare Nothing -> rewriteRuleMKPROC(t)
        TermSquare (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleSQUARE (TermSquare (Just t1))
                Error e -> Error e
        TermCube Nothing -> rewriteRuleMKPROC(t)
        TermCube (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleCUBE (TermCube (Just t1))
                Error e -> Error e
        TermAdd1 Nothing -> rewriteRuleMKPROC(t)
        TermAdd1 (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleADD1 (TermAdd1 (Just t1))
                Error e -> Error e
        TermSub1 Nothing -> rewriteRuleMKPROC(t)
        TermSub1 (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleSUB1 (TermSub1 (Just t1))
                Error e -> Error e
        TermIdentity Nothing -> rewriteRuleMKPROC(t)
        TermIdentity (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleIDENTITY (TermIdentity (Just t1))
                Error e -> Error e
        TermNot Nothing -> rewriteRuleMKPROC(t)
        TermNot (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleNOT (TermNot (Just t1))
                Error e -> Error e
        TermIsZero Nothing -> rewriteRuleMKPROC(t)
        TermIsZero (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleISZERO (TermIsZero (Just t1))
                Error e -> Error e
        TermIsPositive Nothing -> rewriteRuleMKPROC(t)
        TermIsPositive (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleISPOSITIVE (TermIsPositive (Just t1))
                Error e -> Error e
        TermIsNegative Nothing -> rewriteRuleMKPROC(t)
        TermIsNegative (Just arg) ->
            case (evaluateGraph arg) of
                OK t1 -> rewriteRuleISNEGATIVE (TermIsNegative (Just t1))
                Error e -> Error e
        TermList lst ->
            let
                evaldata = List.map evaluateGraph lst
                isError : Result v -> Bool
                isError result =
                    case result of
                        OK _ -> False
                        Error _ -> True
                errors = List.filter isError evaldata
                getLiterals : List (Result Term) -> List Term
                getLiterals elst =
                    case elst of
                        [] -> []
                        (OK x) :: xs -> x :: (getLiterals xs)
                        _ -> [] -- impossible case but make elm compiler happy with _ case
                literals = getLiterals evaldata
            in
            case errors of
                [] -> rewriteRuleEVALLIST (TermList literals)
                x :: xs -> x
        _ -> Error (RRERR_OTHER "unexpected term")

-- TESTS

-- a very small set of testcases (poor coverage as of now) to test basic functionality and core routines
-- import Interpreter and run runTests to get tests summary
runTests =
    let
        arithExample1 = (TermAdd (Just (TermNumeral 5)) (Just (TermNumeral 77)))
        arithExample2 = (TermMultiply (Just (TermAdd (Just (TermNumeral 5)) (Just (TermNumeral 2)))) (Just (TermNumeral 77)))
        mapExample = (TermMap (TermProcedure ([TermSquare Nothing])) (TermLiteralList [1, 5 , -2, 22, -7, -12]))
        filterExample = (TermFilter (TermProcedure ([TermIsPositive Nothing])) (TermLiteralList [1, 5 , -2, 22, -7, -12]))
        reduceExample = (TermReduce (TermProcedure ([TermAdd Nothing Nothing])) (TermNumeral 0) (TermLiteralList [1, 2, 3, 4, 5, 6, 7, 8, 9]))
        compositionExample = (TermMap (TermProcedure ([TermSquare Nothing, TermCube Nothing])) (TermLiteralList [1, 0, 3, -2]))

        testArithExample2 : Result Term
        testArithExample2 =
            case arithExample2 of
                TermMultiply (Just lhs) (Just rhs) ->
                    case lhs of
                        TermAdd (Just a) (Just b) -> 
                            case (rewriteRuleADD lhs) of
                                OK i -> rewriteRuleMULT (TermMultiply (Just i) (Just rhs))
                                Error e -> Error e  
                        _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "expected TermAdd")
                _ -> Error (RRERR_REDEX_MATCH_FAIL_TERM "expected TermMultiply")

        check_equal : Result Term -> Term -> String -> Bool
        check_equal output expected testname =
            let
                result = output == (OK expected)
            in
            Debug.log(testname ++ (if result then " passed." else " failed.") ++ " >> output: " ++ Debug.toString(output))
            result

        results = [(check_equal (evaluateGraph arithExample1) (TermNumeral 82) "eval arithmetic test 1"),
                   (check_equal (evaluateGraph arithExample2) (TermNumeral 539) "eval arithmetic test 2"),
                   (check_equal (evaluateGraph mapExample) (TermLiteralList [1,25,4,484,49,144]) "eval map test"),
                   (check_equal (evaluateGraph filterExample) (TermLiteralList [1,5,22]) "eval filter test"),
                   (check_equal (evaluateGraph reduceExample) (TermNumeral 45) "eval reduce test"),
                   (check_equal (evaluateGraph compositionExample) (TermLiteralList [1, 0, 729, 64]) "eval composition test"),

                   (check_equal (rewriteRuleADD arithExample1) (TermNumeral 82) "arithmetic test 1"),
                   (check_equal testArithExample2 (TermNumeral 539) "arithmetic test 2"),
                   (check_equal (rewriteRuleMAP mapExample) (TermLiteralList [1,25,4,484,49,144]) "map test"),
                   (check_equal (rewriteRuleFILTER filterExample) (TermLiteralList [1,5,22]) "filter test"),
                   (check_equal (rewriteRuleREDUCE reduceExample) (TermNumeral 45) "reduce test"),
                   (check_equal (rewriteRuleMAP compositionExample) (TermLiteralList [1, 0, 729, 64]) "composition test")]

        count_occurrences : a -> List a -> Int
        count_occurrences s slist =
            case slist of
                [] -> 0
                x :: xs -> 
                    if x == s then
                        1 + (count_occurrences s xs)
                    else
                        (count_occurrences s xs)
    in
    String.fromInt (count_occurrences True results) ++ " out of " ++ String.fromInt (List.length results) ++ " tests passed. "
    