module Interface exposing(..)
import Interpreter exposing (..)
import Browser
import Html exposing (Html, h5, input, select, option, p, button, div, text)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Array exposing (..)
import List exposing (..)
import Browser exposing (element)

main =
  Browser.sandbox { init = init, update = update, view = view }

type Tree = 
    Tree {
    value : String
    , children : List Tree
    , root : Bool
    , path : List Int
    }

type alias Model = 
    {
    rootTerm : Term
    , literalArray : List Int
    , tree : Tree
    , mapOrFilter : String
    , arrayStr : String
    , error : String
    }

init : Model
init = { rootTerm = (TermMap (TermCompose (TermSquare Nothing) (TermCube Nothing)) (TermLiteralList [1, 5 , -2, 22, -7, -12]))
    , literalArray = [1, 5 , -2, 22, -7, -12]
    , tree = Tree { 
        value = "None"
        , children = []
        , root = True
        , path = []
        } 
    , mapOrFilter = "Map"
    , arrayStr = (makeStrFromArray [1, 5 , -2, 22, -7, -12])
    , error = ""
    }

type Msg = Add (List Int) String | ChangeMoF String | UpdateArray String | StoreArrayStr String | Evaluate

update : Msg -> Model -> Model
update msg model =
    case msg of
        Add path str ->
            let
                log = (Debug.log str path)
            in
                { model | tree = (insert model.tree path str) }
        ChangeMoF str ->
            let
                log = (Debug.log ("New :" ++ str ++ ", Old :" ++ model.mapOrFilter) str)
            in
                { model | mapOrFilter = str }
        StoreArrayStr str ->
            { model | arrayStr = str }
        UpdateArray str ->
            { model | literalArray = (makeArrFromString str) }
        Evaluate -> modelEvaluate {model | error = ""}


toAddMsg : (List Int) -> String -> Msg
toAddMsg path str = Add path str

--toUpdateArrMsg : String ->

view : Model -> Html Msg
view model =
    let { rootTerm , literalArray , tree } = model
    in
        div [] [
            div [class "uk-card uk-card-default uk-card-small uk-card-body uk-margin "] [
                h5 [class "uk-card-title"] [text model.mapOrFilter],
                button [class "uk-button uk-button-primary", onClick (ChangeMoF "Map")] [text "Map"],
                button [class "uk-button uk-button-primary uk-margin-left", onClick (ChangeMoF "Filter")] [text "Filter"],

                div [ class "uk-card uk-card-default uk-card-small uk-card-body uk-margin" ] [
                    h5 [class "uk-card-title"] [text "Input List"]
                    , p [] [text (makeStrFromArray model.literalArray)]
                    , input [class "uk-input", onInput (StoreArrayStr), placeholder (makeStrFromArray model.literalArray)] []
                    , button [ class "uk-button uk-button-primary uk-margin" ,onClick (UpdateArray model.arrayStr) ] [text "Update Array"]
                ]
                , (treeToHtml tree)
                , div [] [
                    p [class "uk-text-danger uk-margin-left"] [text (model.error)],
                    button [ class "uk-button uk-button-primary", onClick Evaluate ] [ text "Evaluate" ]
                ]
            ]
        ]

makeStrFromArray : (List Int) -> String
makeStrFromArray arr =
    if List.isEmpty arr then
        "[]"
    else
        String.join "," (List.map String.fromInt arr)

makeArrFromString : String -> List Int
makeArrFromString str =
    List.filterMap String.toInt (String.split "," str)

modelEvaluate : Model -> Model
modelEvaluate model = 
    let
        term = treeToTerm model.tree
        validAST =
            if model.mapOrFilter == "Filter" then
                case (treeReturnsBool model.tree False) of
                    Just b -> b
                    Nothing -> True
            else
                True
    in
    if not validAST then
        {model | error = "Filter function must return boolean values."}
    else
        case term of
            Just t ->
                if model.mapOrFilter == "Map" then
                    let 
                        res = evaluateGraph (TermMap (t) (TermLiteralList model.literalArray) )
                    in
                        case res of
                            OK t1 ->
                                case t1 of
                                    TermLiteralList arr ->
                                        { model | literalArray = arr }
                                    _ ->
                                        model
                            Error e ->
                                { model | error = getErrorString e}
                else
                    let 
                        res = evaluateGraph (TermFilter (t) (TermLiteralList model.literalArray) )
                    in
                        case res of
                            OK t2 ->
                                case t2 of 
                                    TermLiteralList arr ->
                                        { model | literalArray = arr }
                                    _ ->
                                        model
                            Error e ->
                                { model | error = getErrorString e}
            Nothing ->
                { model | error = "Invalid AST"}

treeToHtml : Tree -> Html Msg
treeToHtml (Tree tree) = 
    div [ class "uk-card uk-card-default uk-card-small uk-card-body uk-margin" ]
        ([  select [ class "uk-select", placeholder "Select procedure", onInput (toAddMsg tree.path)] [
              option [ value "None", selected (tree.value=="None") ] [text "None"]
              , option [ value "Compose", selected (tree.value=="Compose") ] [text "Compose"]
              , option [ value "Square", selected (tree.value=="Square") ] [text "Square"]
              , option [ value "Cube", selected (tree.value=="Cube") ] [text "Cube"]
              , option [ value "Add1", selected (tree.value=="Add1") ] [text "Add1"]
              , option [ value "Sub1", selected (tree.value=="Sub1") ] [text "Sub1"]
              , option [ value "Identity", selected (tree.value=="Identity") ] [text "Identity"]
              , option [ value "Not", selected (tree.value=="Not") ] [text "Not"]
              , option [ value "IsZero?", selected (tree.value=="IsZero?") ] [text "IsZero?"]
              , option [ value "IsNegative?", selected (tree.value=="IsNegative?") ] [text "IsNegative?"]
              , option [ value "IsPositive?", selected (tree.value=="IsPositive?") ] [text "IsPositive?"]
            ]
        ] ++ ( List.map treeToHtml tree.children ))

getValAt : Int -> List a -> Maybe a
getValAt index lst = 
    List.head (List.drop index lst)

treeToTerm : Tree -> Maybe Term
treeToTerm (Tree tree) = 
    if tree.value == "None" then
        Nothing
    else if tree.value == "Compose" then
        case (getValAt 0 tree.children) of
            Just child1 ->
                case (treeToTerm child1) of
                    Just t1 ->
                        case (getValAt 1 tree.children) of
                            Just child2 ->
                                case (treeToTerm child2) of
                                    Just t2 ->
                                        (Just (TermCompose t1 t2))
                                    Nothing ->
                                        Nothing
                            Nothing -> Nothing
                    Nothing -> Nothing
            Nothing -> Nothing
    else
        if tree.value == "Square" then
            Just (TermSquare Nothing)
        else if tree.value == "Cube" then
            Just (TermCube Nothing)
        else if tree.value == "Add1" then
            Just (TermAdd1 Nothing)
        else if tree.value == "Sub1" then
            Just (TermSub1 Nothing)
        else if tree.value == "Identity" then
            Just (TermIdentity Nothing)
        else if tree.value == "Not" then
            Just (TermNot Nothing)
        else if tree.value == "IsZero?" then
            Just (TermIsZero Nothing)
        else if tree.value == "IsNegative?" then
            Just (TermIsNegative Nothing)
        else if tree.value == "IsPositive?" then
            Just (TermIsPositive Nothing)
        else 
            Nothing
            
treeReturnsBool : Tree -> Bool -> Maybe Bool
treeReturnsBool (Tree tree) inputBool = 
    if tree.value == "None" then
        Nothing
    else if tree.value == "Compose" then
        case (getValAt 0 tree.children) of
            Just child1 ->
                case (treeReturnsBool child1 inputBool) of
                    Just t1 ->
                        case (getValAt 1 tree.children) of
                            Just child2 -> (treeReturnsBool child2 t1)
                            Nothing -> Nothing
                    Nothing -> Nothing
            Nothing -> Nothing
    else
        if tree.value == "Square" || tree.value == "Cube" || tree.value == "Add1" || tree.value == "Sub1" then
            if inputBool then
                Nothing
            else
                Just False
        else if tree.value == "Identity" then
            Just inputBool
        else if tree.value == "Not" then
            if inputBool then
                Just True
            else
                Nothing
        else if tree.value == "IsZero?" || tree.value == "IsNegative?" || tree.value == "IsPositive?"then
            if inputBool then
                Nothing
            else
                Just True
        else 
            Nothing

insert : Tree -> List Int -> String -> Tree
insert (Tree tree) path str =
    case path of
        [] ->
            if str == "Compose" then
                let 
                    newChild1 = Tree { value = "None" , children = [], root = False, path = tree.path ++ [0] }
                    newChild2 = Tree { value = "None" , children = [], root = False, path = tree.path ++ [1] }
                in 
                    Tree {
                        value = str
                        , children = [newChild1, newChild2]
                        , root = tree.root
                        , path = tree.path
                    }
            else
                Tree {
                    value = str
                    , children = []
                    , root = tree.root
                    , path = tree.path
                }

        x::xs ->
            let 
                toggle idx (Tree tree1) =
                    if x==idx then
                        (insert (Tree tree1) xs str)
                    else
                        (Tree tree1)
            in
                let
                    children = tree.children
                    b = List.indexedMap toggle children
                in 
                    Tree {
                        value = tree.value
                        , children = b
                        , root = tree.root
                        , path = tree.path
                    }
