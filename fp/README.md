# Instructions

## To Run the System
- Run `elm reactor` in current directory
- Go to `localhost:8000/src/Interface.elm`

## To Play around with Underlying Term Graphs Setup
- Run `elm repl`
- Execute `import Main exposing (..)`
- Execute `runTests` to run some predefined tests.
- Do whatever you want; make term graphs, apply rewrite rules, evaluate whole term graphs, etc