module Graph exposing (..)
import Array exposing (..)
import Html exposing (q)
import Html exposing (node)
import Html exposing (i)
import Json.Encode as Encode

type Status = Visited | Visible | Unvisited
type GraphNode = Node Int String (List Int) Status

type Graph = AdjacencyList (Array.Array GraphNode)

type Path = Path (List GraphNode)

addToPath : GraphNode -> Path -> Path
addToPath node path =
  case path of
    Path nodes -> Path (List.append nodes (List.singleton node))

makeVisible : List Int -> Graph -> Graph
makeVisible nodes g =
  case nodes of
    [] -> g
    first :: rest ->
      case g of
        AdjacencyList alist ->
          let
            v = (Array.get first alist)
          in
            case v of
              Just node ->
                case node of
                  Node id label neighbours status ->
                    case (makeVisible rest g) of
                      AdjacencyList newAdjList ->
                        case status of
                          Visited ->  AdjacencyList (Array.set first (Node id label neighbours Visited) newAdjList)
                          Visible ->  AdjacencyList (Array.set first (Node id label neighbours Visible) newAdjList)
                          Unvisited ->  AdjacencyList (Array.set first (Node id label neighbours Visible) newAdjList)
              Nothing -> (makeVisible rest g)

visitNode : Graph -> Int -> Graph
visitNode g i =
  case g of
    AdjacencyList alist ->
      let
        v = (Array.get i alist)
      in
        case v of
          Just node ->
            case node of
              Node id label neighbours status ->
                case (makeVisible neighbours g) of
                  AdjacencyList newAdjList ->
                    AdjacencyList (Array.set i (Node id label neighbours Visited) newAdjList)
          Nothing -> AdjacencyList alist

visitNodeSansVisible: Graph -> Int -> Graph
visitNodeSansVisible g i = 
  case g of
     AdjacencyList alist ->
      let
          v = (Array.get i alist)
      in
        case v of 
          Just node ->
            case node of 
              Node id label neighbours status -> 
                case g of
                  AdjacencyList newAdjList ->
                    AdjacencyList (Array.set i (Node id label neighbours Visited) newAdjList)
          Nothing -> AdjacencyList alist       
      

nodeLabel : Graph -> Int -> String
nodeLabel g i =
  case g of
    AdjacencyList alist ->
      let
        v = (Array.get i alist)
      in
        case v of
          Just node ->
            case node of
              Node id label neighbours status -> label
          Nothing -> ""

checkVisited : Graph -> Int -> Bool
checkVisited g i =
  case g of
    AdjacencyList alist ->
      let
        v = (Array.get i alist)
      in
        case v of
          Just node ->
            case node of 
              Node id label children status ->
                case status of
                  Visited -> True
                  Visible -> False
                  Unvisited -> False
          Nothing -> False

checkVisible : Graph -> Int -> Bool
checkVisible g i =
  case g of
    AdjacencyList alist ->
      let
        v = (Array.get i alist)
      in
        case v of
          Just node ->
            case node of 
              Node id label children status ->
                case status of
                  Visited -> False
                  Visible -> True
                  Unvisited -> False
          Nothing -> False

allNeighboursVisited : Graph -> Int -> Bool
allNeighboursVisited g i =
  case g of
    AdjacencyList alist ->
      let
        v = (Array.get i alist)
      in
        case v of
          Just node ->
            case node of
              Node id label children status -> List.foldl (&&) True (List.map (\c -> checkVisited g c) children)
          Nothing -> False

nodeToJson : GraphNode -> List (String, Encode.Value)
nodeToJson n =
  case n of 
    Node id label neighbours status -> [("id", Encode.int id), ("name", Encode.string label)]

neighboursToJson : GraphNode -> List (List (String, Encode.Value))
neighboursToJson n =
  case n of 
    Node id label neighbours status -> 
      List.map (\x -> [("source", Encode.int id), ("target", Encode.int x)]) neighbours

toJson : Graph -> Encode.Value
toJson g =
  case g of
    AdjacencyList alist ->
      Encode.object 
        [("nodes", Encode.list Encode.object (Array.toList (Array.map nodeToJson alist))), 
        ("links", Encode.list Encode.object (List.concatMap neighboursToJson (Array.toList alist)))
        ]

updateFirstVisitedNeighbour : Array (Maybe Int) -> Graph -> Int -> Array (Maybe Int)
updateFirstVisitedNeighbour firstVisitedNeighbour g i =
  Array.indexedMap (\y -> \n ->
                            case n of
                              Just j -> Just j
                              Nothing ->
                                case g of
                                  AdjacencyList alist ->
                                    case (Array.get y alist) of
                                      Just v ->
                                        case v of
                                          Node id label neighbours status ->
                                            case (List.filter (\x -> x == i) neighbours) of
                                              [] -> n
                                              first :: rest -> Just i
                                      Nothing -> n) firstVisitedNeighbour