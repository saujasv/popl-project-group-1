module BFS exposing (..)
import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Array exposing (..)
import List exposing (..)
import Browser exposing (element)
import Graph exposing (..)
import SpanningTree exposing (..)
import Html exposing (i)
import Html exposing (s)
import Html exposing (q)
import Html exposing (node)
import Json.Encode exposing(..)
-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }

-- MODEL

type alias Model = 
  {
    graph : Graph
    , queue: List Int
    , spanningTree : Maybe TreeNode
    , currentNode : Maybe Int
    , firstVisitedNeighbour : Array (Maybe Int)
  }

init : Model
init =
  {
    graph = 
      AdjacencyList (Array.fromList
        [
          Graph.Node 0 "A" [1, 2] Graph.Visited
          , Graph.Node 1 "B" [0, 3, 4] Graph.Visible
          , Graph.Node 2 "C" [0, 4, 5] Graph.Visible
          , Graph.Node 3 "D" [1] Graph.Unvisited
          , Graph.Node 4 "E" [1, 2] Graph.Unvisited
          , Graph.Node 5 "F" [2] Graph.Unvisited
        ]
      )
    , queue = [0]
    , spanningTree = Just (LeafNode 0 "A")
    , currentNode = Just 0
    , firstVisitedNeighbour = (Array.fromList [Nothing, Just 0, Just 0, Nothing, Nothing, Nothing])
  }


-- UPDATE

updateQueue : List Int -> Int -> Graph -> List Int
updateQueue q i g =
  case q of 
    [] -> []
    first :: rest ->
      if (allNeighboursVisited g first) then
        (updateQueue rest i g)
      else 
        List.append q [i]

type Msg = Select Int | Restart

update : Msg -> Model -> Model
update msg model =
  case msg of
    Select i ->
      if (checkVisible model.graph i) then
        { model |
          -- graph = Graph.visitNode model.graph i
          graph = case model.queue of 
                    [] -> model.graph
                    first :: rest ->
                      if (allNeighboursVisited (Graph.visitNodeSansVisible model.graph i) first) then
                        case rest of
                           [] -> model.graph
                           x::xs -> Graph.visitNode (Graph.visitNodeSansVisible model.graph i) x
                      else
                        (Graph.visitNodeSansVisible model.graph i)
          , currentNode = Just i
          , queue = if (checkQueue model.graph model.queue i) then
                      (updateQueue model.queue i (Graph.visitNodeSansVisible model.graph i))
                    else
                      (updateQueue model.queue i (Graph.visitNode model.graph i))
          , spanningTree = (case model.spanningTree of
                              Just tree -> 
                                case (Array.get i model.firstVisitedNeighbour) of
                                  Just x -> 
                                    case x of
                                      Just y -> Just (SpanningTree.update tree y i (Graph.nodeLabel model.graph i))
                                      Nothing -> model.spanningTree
                                  Nothing -> model.spanningTree
                              Nothing -> Just (LeafNode i (Graph.nodeLabel model.graph i)))
          , firstVisitedNeighbour = (updateFirstVisitedNeighbour model.firstVisitedNeighbour model.graph i)
        }
      else
        model
    Restart -> init

checkQueue : Graph -> List Int -> Int ->Bool
checkQueue g qu i =
  case g of
     AdjacencyList alist ->
      case qu of
        [] -> False
        first :: rest ->
          let
            v = (Array.get first alist)
              in
                case v of
                  Just node ->
                    case node of
                      Node id label children status -> (List.member i children)
                  Nothing -> False
        
        
      
-- VIEW

view : Model -> Html Msg
view model =
  Html.div [style "text-align" "center", style "margin-top" "56px"] (viewArray model.graph model.currentNode ++ [
    Html.div [style "margin-top" "20px"] [ 
      -- Html.p [] [ text "Selected node is " , Html.b [style "color" "#e39d10"] [text "in this color"] ]
    -- , Html.p [] [ text "Partially visited nodes are " , Html.b [style "color" "#d60656"] [text "in this color"] ]
    Html.p [] [ text "Visited nodes are " , Html.b [style "color" "#ed3f00"] [text "in this color"] ]
    , Html.p [] [ text "Possible to visit nodes are " , Html.b [style "color" "#0eeb8b"] [text "in this color"] ]
    , Html.p [] [ text "Not Possible to Visit nodes are " , Html.b [style "color" "#0357ff"] [text "in this color"] ]
    , Html.div [style "text-align" "center"] ([ Html.h4 [] [ text "Queue : "]] ++ (viewQueue 
                                                                                    (List.map 
                                                                                      (\i -> (case model.graph of
                                                                                                AdjacencyList alist -> 
                                                                                                  case (Array.get i alist) of
                                                                                                    Just node ->
                                                                                                      case node of
                                                                                                        Node id label neighbours status -> label
                                                                                                    Nothing -> "")) 
                                                                                      model.queue)) )
    , Html.br [] []
    , Html.div [] [ Html.button [class "uk-button uk-button-primary", onClick Restart] [text "Restart" ] ]
    , Html.p [id "bfs-ts2-graph-json", Html.Attributes.hidden True] [text (Json.Encode.encode 0 (Graph.toJson model.graph)) ]
    , Html.p [id "bfs-ts2-tree-json", Html.Attributes.hidden True] [text (Json.Encode.encode 0 (SpanningTree.toJson model.spanningTree))]] 
    ])

viewQueue : List String -> List (Html Msg)
viewQueue queue = 
  let
    log = Debug.log "Visited Arr Display Err"
  in
    (List.map (\element -> (viewElement element)) queue)

viewArray : Graph -> Maybe Int -> List (Html Msg)
viewArray graph sel =
  case sel of
    Just i ->
      case graph of
        AdjacencyList alist -> (Array.toList (Array.map (\element -> (viewButton element i graph)) alist))
    Nothing ->
      case graph of
        AdjacencyList alist -> (Array.toList (Array.map (\element -> (viewInit element)) alist))

viewElement : String -> Html Msg
viewElement x = 
  button [style "background-color" "#c107eb"
  , style "border-radius" "50%"
  , style "margin" "5px"
  , style "width" "50px"
  , style "height" "50px"
  , style "text-align" "center"]
  [ text x ] 


viewInit : GraphNode -> Html Msg
viewInit node =
  case node of
    Graph.Node id label children status ->
      button [ onClick (Select id)
              , style "background-color" "#c107eb"
              , style "border-radius" "50%"
              , style "margin" "5px"
              , style "width" "50px"
              , style "height" "50px"
              , style "text-align" "center"]
              [ text (label) ]

viewButton : GraphNode -> Int -> Graph -> Html Msg
viewButton node sel g =
  case node of
    Graph.Node id label children status ->
      case status of
        Graph.Visited -> 
          button [ onClick (Select id)
                  , style "background-color" "#ed3f00"
                  , style "border-radius" "50%"
                  , style "margin" "5px"
                  , style "width" "50px"
                  , style "height" "50px"
                  , style "text-align" "center"]
                  [ text (label) ]
        Graph.Visible -> 
          button [ onClick (Select id)
          , style "background-color" "#0eeb8b"
          , style "border-radius" "50%"
          , style "margin" "5px"
          , style "width" "50px"
          , style "height" "50px"
          , style "text-align" "center"]
          [ text (label) ]
        Graph.Unvisited -> 
          button [ onClick (Select id)
                  , style "background-color" "#0357ff"
                  , style "border-radius" "50%"
                  , style "margin" "5px"
                  , style "width" "50px"
                  , style "height" "50px"
                  , style "text-align" "center"
                  , disabled True]
                  [ text (label) ]