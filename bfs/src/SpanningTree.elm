module SpanningTree exposing (..)
import Json.Encode as Encode

type TreeNode =
  InternalNode Int String (List TreeNode)
  | LeafNode Int String

updateChildren : List TreeNode -> Int -> Int -> String -> (List TreeNode)
updateChildren children source childId childLabel =
  case children of
    [] -> []
    first :: rest ->
      (update first source childId childLabel) :: (updateChildren rest source childId childLabel)

update : TreeNode -> Int -> Int -> String -> TreeNode
update tree source childId childLabel =
  case tree of 
    InternalNode id label children ->
      if id == source then
        InternalNode id label (List.append children (List.singleton (LeafNode childId childLabel)))
      else
        InternalNode id label (updateChildren children source childId childLabel)
    LeafNode id label ->
      if id == source then
        InternalNode id label [(LeafNode childId childLabel)]
      else
        LeafNode id label

getNodes : TreeNode -> List (Int, String)
getNodes tree =
  case tree of
    InternalNode id label children -> List.concat [[(id, label)], (List.concatMap getNodes children)]
    LeafNode id label -> [(id, label)]

getEdges : TreeNode -> List (Int, Int)
getEdges tree = 
  case tree of
    InternalNode id label children -> List.concat [(List.map (\c -> case c of
                                                                InternalNode cid clabel cchildren -> (id, cid)
                                                                LeafNode cid clabel -> (id, cid)) 
                                                    children), 
                                                    (List.concatMap getEdges children)]
    LeafNode id label -> []

toJson : Maybe TreeNode -> Encode.Value
toJson t =
  case t of
    Just tree ->
      Encode.object [("nodes", Encode.list Encode.object (List.map
                                    (\x -> 
                                        [("id", Encode.int (Tuple.first x)), 
                                          ("name", Encode.string (Tuple.second x))]
                                          ) 
                                  (getNodes tree))), 
                      ("links", Encode.list Encode.object (List.map (\x -> 
                                        [("source", Encode.int (Tuple.first x)), 
                                          ("target", Encode.int (Tuple.second x))]
                                          ) (getEdges tree)))]
    Nothing -> Encode.object [("nodes", Encode.list Encode.object []), 
                      ("links", Encode.list Encode.object [])]