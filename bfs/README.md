Do `elm reactor` in the current directory then go to http://127.0.0.1:8000/src/Main.elm

Test graph is :

 V = [1,2,3,4,5,6]

E = [[1,2], [1,3], [2,4], [2,5], [3,5], [3,6]]