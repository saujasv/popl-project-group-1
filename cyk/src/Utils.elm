module Utils exposing (..)
import Maybe exposing (Maybe)

type TableIndex = Pair (Maybe Int) (Maybe Int)

firstIndex : TableIndex -> Maybe Int
firstIndex t =
  case t of
    Pair i j -> 
      case i of
        Just n -> Just n
        Nothing -> Nothing

secondIndex : TableIndex -> Maybe Int
secondIndex t =
  case t of
    Pair i j -> 
      case j of
        Just n -> Just n
        Nothing -> Nothing

indexToString : Maybe Int -> String
indexToString x =
  case x of
    Just n -> String.fromInt n
    Nothing -> ""