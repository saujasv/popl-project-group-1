module TableFilling exposing (..)
import Browser
import Html exposing (Html, text, pre, button, div, input, p, ul, li)
import Html exposing (table, tr, td)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (..)
import Http
import Array
import Maybe exposing (Maybe)
import CFG exposing (..)
import Utils exposing (..)
import Table exposing (..)

-- MAIN

main =
  Browser.sandbox { init = init, update = update, view = view }

-- MODEL

-- grammar : Grammar
-- grammar = Grammar ["S", "NP", "VP", "N", "V", "Det"] 
--                   ["a", "the", "cat", "dog", "mouse", "chases", "likes"] 
--                   (Array.fromList 
--                   [ NonTerminalRule "S" "NP" "VP"
--                     , NonTerminalRule "NP" "Det" "N"
--                     , NonTerminalRule "VP" "V" "NP"
--                     , TerminalRule "Det" "the"
--                     , TerminalRule "Det" "a"
--                     , TerminalRule "N" "cat"
--                     , TerminalRule "N" "dog"
--                     , TerminalRule "N" "mouse"
--                     , TerminalRule "V" "chases"
--                     , TerminalRule "V" "likes"
--                   ])
--                   "S"

type alias Model = 
  {
    string : Array.Array String
    , table : CYKTable
    , cell1 : TableIndex
    , cell2 : TableIndex
    , targetCell : TableIndex
    , errorState : Bool
  }

str : Array.Array String
str = Array.fromList ["the", "cat", "chases", "the", "mouse"]
init : Model
init = Model 
        str
        (Table.initialize str grammar)
        (Pair (Just -1) (Just -1))
        (Pair (Just -1) (Just -1))
        (Pair (Just -1) (Just -1))
        False

-- UPDATE

type Msg
  = Index Int Int String
  | Transition

constructIndexMsg : Int -> Int -> (String -> Msg)
constructIndexMsg c i = (\s -> Index c i s)

matchProduction : Production -> Production -> Production -> Bool
matchProduction p1 p2 p3 = 
  case p3 of
    TerminalRule lhs3 rhs3 -> False
    NonTerminalRule lhs3 rhs31 rhs32 -> 
      case p1 of
        TerminalRule lhs1 rhs1 ->
          case p2 of
            TerminalRule lhs2 rhs2 -> lhs1 == rhs31 && lhs2 == rhs32
            NonTerminalRule lhs2 rhs21 rhs22 -> lhs1 == rhs31 && lhs2 == rhs32
        NonTerminalRule lhs1 rhs11 rhs12 ->
          case p2 of
            TerminalRule lhs2 rhs2 -> lhs1 == rhs31 && lhs2 == rhs32
            NonTerminalRule lhs2 rhs21 rhs22 -> lhs1 == rhs31 && lhs2 == rhs32

reduceProductionPair : Grammar -> Production -> Production -> List Production
reduceProductionPair g p1 p2 =
    Array.toList (Array.filter (\p -> matchProduction p1 p2 p) g.productions)

reduceProduction : Grammar -> Production -> List Production -> List Production
reduceProduction g p1 l = List.concatMap (\p -> reduceProductionPair g p1 p) l

reduce : Grammar -> List Production -> List Production -> List Production
reduce g p1 p2 = List.concatMap (\p -> reduceProduction g p p2) p1

checkIndices : TableIndex -> TableIndex -> TableIndex -> Bool
checkIndices cell1 cell2 targetCell =
  if (firstIndex cell1) == (firstIndex targetCell) then
    if (secondIndex cell2) == (secondIndex targetCell) then
      case (secondIndex cell1) of
        Just i ->
          case (firstIndex cell2) of
            Just j -> if i + 1 == j then True else False
            Nothing -> False
        Nothing -> False
    else
      False
  else
    False


transition : CYKTable -> TableIndex -> TableIndex -> TableIndex -> Grammar -> CYKTable
transition cyktable cell1 cell2 targetCell g = 
  (updateCell 
    cyktable 
    targetCell 
    (reduce 
      g 
      (getCell cyktable cell1) 
      (getCell cyktable cell2)))
  
update : Msg -> Model -> Model
update msg model = 
  case msg of 
    Index c i s -> 
      if c == 0 then
        if i == 1 then
          {model | targetCell = Pair (case (String.toInt s) of
                                  Just n -> Just n
                                  Nothing -> Nothing) (secondIndex model.targetCell) }
        else
          {model | targetCell = Pair (firstIndex model.targetCell) (case (String.toInt s) of
                                                            Just n -> Just n
                                                            Nothing -> Nothing) }
      else if c == 1 then
        if i == 1 then
          {model | cell1 = Pair (case (String.toInt s) of
                                  Just n -> Just n
                                  Nothing -> Nothing) (secondIndex model.cell1) }
        else
          {model | cell1 = Pair (firstIndex model.cell1) (case (String.toInt s) of
                                                            Just n -> Just n
                                                            Nothing -> Nothing) }
      else
        if i == 1 then
          {model | cell2 = Pair (case (String.toInt s) of
                                  Just n -> Just n
                                  Nothing -> Nothing) (secondIndex model.cell2) }
        else
          {model | cell2 = Pair (firstIndex model.cell2) (case (String.toInt s) of
                                                            Just n -> Just n
                                                            Nothing -> Nothing) }
    Transition -> if (checkIndices model.cell1 model.cell2 model.targetCell) then
                    {model | table = (transition model.table model.cell1 model.cell2 model.targetCell grammar), errorState = False}
                  else
                    {model | errorState = True}

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-- VIEW

view : Model -> Html Msg
view model =
  div [] [
    div [class "uk-grid-divider uk-child-width-expand@s uk-grid"] [
          div [class "uk-dark"] [(Table.toHtml model.table)]
        , div [class "uk-dark"] [ ul [class "uk-list"] (List.map (\line -> li [] [text line]) (Array.toList (Array.map toString grammar.productions)))]]
    , div [class "uk-margin"] [
        div [class "uk-padding-small"] [
          p [] [text "Select target cell coordinates:"],
          input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (firstIndex model.targetCell)), onInput (constructIndexMsg 0 1) ] [],
          input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (secondIndex model.targetCell)), onInput (constructIndexMsg 0 2) ] []]
      , div [class "uk-flex uk-margin"] [
          div [class "uk-padding-small"] [
            p [] [text "Select first source cell coordinates:"],
            input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (firstIndex model.cell1)), onInput (constructIndexMsg 1 1) ] [],
            input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (secondIndex model.cell1)), onInput (constructIndexMsg 1 2) ] []]
        , div [class "uk-padding-small"] [
            p [] [text "Select second source cell coordinates:"],
            input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (firstIndex model.cell2)), onInput (constructIndexMsg 2 1) ] [],
            input [class "uk-input uk-form-width-small", type_ "number", placeholder "0", value (indexToString (secondIndex model.cell2)), onInput (constructIndexMsg 2 2) ] []]]]
      , p [class "uk-text-danger uk-margin-left"] [text (if model.errorState then "Bad selection of cells." else "")]
      , button [ class "uk-button uk-button-primary uk-margin-left", onClick Transition ] [ text "Update Table" ]
    ]