module Table exposing (..)
import Array
import CFG exposing (..)
import Utils exposing (..)
import Html
import Html.Attributes
import Array exposing (Array)

type CYKTable = Table (Array.Array (Array.Array (List Production)))

getCell : CYKTable -> TableIndex -> List Production
getCell cyktable index =
  case cyktable of
    Table table ->
      let
        x = (firstIndex index)
        y = (secondIndex index)
      in
        case x of
          Just i ->
            case y of
              Just j ->
                case (Array.get i table) of
                  Just row ->
                    case (Array.get j row) of
                      Just cell -> cell
                      Nothing -> []
                  Nothing -> []
              Nothing -> []
          Nothing -> []

setCell : CYKTable -> TableIndex -> List Production -> CYKTable
setCell cyktable index prod =
  case cyktable of
    Table table ->
      let
        x = (firstIndex index)
        y = (secondIndex index)
      in
        case x of
          Just i ->
            case y of
              Just j ->
                case (Array.get i table) of
                  Just row -> Table (Array.set i (Array.set j prod row) table)
                  Nothing -> Table table
              Nothing -> Table table
          Nothing -> Table table

updateCell : CYKTable -> TableIndex -> List Production -> CYKTable
updateCell cyktable index prod =
  case cyktable of
    Table table ->
      let
        x = (firstIndex index)
        y = (secondIndex index)
      in
        case x of
          Just i ->
            case y of
              Just j ->
                case (Array.get i table) of
                  Just row -> 
                    case (Array.get j row) of
                      Just cell -> Table (Array.set i (Array.set j (List.append cell prod) row) table)
                      Nothing -> Table table
                  Nothing -> Table table
              Nothing -> Table table
          Nothing -> Table table

getTerminalMatches : String -> Grammar -> List Production
getTerminalMatches s g = 
  (Array.toList 
    (Array.filter 
      (\p ->
        case p of
          TerminalRule lhs rhs -> rhs == s
          NonTerminalRule lhs rhs1 rhs2 -> False
          ) 
      g.productions))

blankTable : Int -> CYKTable
blankTable len = (Table (Array.repeat len (Array.repeat len [])))

fillInitialCells : List (List Production) -> Int -> CYKTable -> CYKTable
fillInitialCells initial i table =
  case (List.head initial) of
    Just head ->
      case (List.tail initial) of
        Just rest -> (fillInitialCells rest (i + 1) (updateCell table (Pair (Just i) (Just i)) head))
        Nothing -> (updateCell table (Pair (Just i) (Just i)) head)
    Nothing -> table

initialize : Array.Array String -> Grammar -> CYKTable
initialize s g = 
  let
    terminalMatches = (Array.toList (Array.map (\c -> (getTerminalMatches c g)) s))
  in
    fillInitialCells terminalMatches 0 (blankTable (Array.length s))

cellToHtml : List Production -> Html.Html msg
cellToHtml cell = (Html.td [Html.Attributes.class "uk-background-muted uk-padding-small uk-text-small uk-margin"] (List.map (\line -> Html.div [] [Html.text line]) (List.map toString cell)))

rowToHtml : Array.Array (List Production) -> Html.Html msg
rowToHtml row = (Html.tr [] (Array.toList (Array.map cellToHtml row)))

toHtml : CYKTable -> Html.Html msg
toHtml cyktable =
  case cyktable of
    Table table -> (Html.table [] (Array.toList (Array.map rowToHtml table)))
