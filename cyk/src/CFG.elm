module CFG exposing (..)
import Array

type Production
  = TerminalRule String String
  | NonTerminalRule String String String

type alias Grammar = 
  {
    variables : List String
    , alphabet : List String
    , productions : Array.Array Production
    , start : String
  }

toString : Production -> String
toString prod =
  case prod of
    TerminalRule lhs rhs -> String.join " " [lhs, "→", rhs]
    NonTerminalRule lhs rhs1 rhs2 -> String.join " " [lhs, "→", rhs1, rhs2]

grammar = Grammar ["Sentence", "NounPhrase", "VerbPhrase", "Noun", "Verb", "Determiner"] 
                  ["a", "the", "cat", "dog", "mouse", "chases", "likes"] 
                  (Array.fromList 
                  [ NonTerminalRule "Sentence" "NounPhrase" "VerbPhrase"
                    , NonTerminalRule "NounPhrase" "Determiner" "Noun"
                    , NonTerminalRule "VerbPhrase" "Verb" "NounPhrase"
                    , TerminalRule "Determiner" "the"
                    , TerminalRule "Determiner" "a"
                    , TerminalRule "Noun" "cat"
                    , TerminalRule "Noun" "dog"
                    , TerminalRule "Noun" "mouse"
                    , TerminalRule "Verb" "chases"
                    , TerminalRule "Verb" "likes"
                  ])
                  "S"