module Reduction exposing (..)
import Browser
import Html exposing (Html, text, pre, button, div, input, p)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (..)
import Http
import Array
import Maybe exposing (Maybe)
import CFG exposing (..)
import Html exposing (h1)
import CFG exposing (grammar)

-- MAIN

main =
  Browser.sandbox { init = init, update = update, view = view }

-- MODEL

type alias Model = 
  {
    state : Array.Array String
    , production_number : Maybe Int
    , index : Maybe Int
  }
init : Model
init = Model (Array.fromList ["the", "dog", "chases", "the", "cat"]) (Just -1) (Just -1) 


-- UPDATE

transition : (Array.Array String) -> Int -> Maybe Production -> (Array.Array String)
transition s i mprod = 
  case mprod of
    Just prod ->
      case prod of
        TerminalRule lhs rhs -> 
          case (Array.get i s) of
            Just c -> 
              if c == rhs then
                Array.set i lhs s
              else
                s
            Nothing -> s
        NonTerminalRule lhs rhs1 rhs2 ->
          case (Array.get i s) of
            Just c1 -> 
              case (Array.get (i + 1) s) of
                Just c2 -> 
                  if c1 == rhs1 && c2 == rhs2 then 
                    Array.append 
                      (Array.slice 0 i s) 
                      (Array.append 
                        (Array.fromList [lhs]) 
                        (Array.slice (i + 2) (Array.length s) s))
                  else
                    s
                Nothing -> s
            Nothing -> s
    Nothing -> s

type Msg
  = ProductionNumber Int
  | Index Int
  | Transition

update : Msg -> Model -> Model
update msg model = 
  case msg of 
    ProductionNumber p -> { model | production_number = Just p }
    Index i -> { model | index = Just i }
    Transition -> Model 
                  (transition 
                    model.state 
                    (case model.index of
                      Just n -> n
                      Nothing -> -1)
                    (Array.get 
                      (case model.production_number of
                        Just n -> n
                        Nothing -> -1)
                      grammar.productions)) 
                  (Just -1)
                  (Just -1)

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-- VIEW

buttonStyle = style "text-transform" "none"

renderStringCharacter : List String -> Int -> List (Html Msg)
renderStringCharacter string index =
  let
    renderButton : Int -> Bool -> String -> (Html Msg)
    renderButton v marked txt =
      if marked then
        button [class "uk-button uk-button-primary", buttonStyle, onClick (Index v)] [text txt]
      else
        button [class "uk-button uk-button-default", buttonStyle, onClick (Index v)] [text txt]

    generateButtons lst i =
      case lst of
        [] -> []
        (x :: xs) -> 
          if index /= -1 && (index == i || index + 1 == i) then
            (renderButton i True x) :: (generateButtons xs (i + 1))
          else
            (renderButton i False x) :: (generateButtons xs (i + 1))
  in
    generateButtons string 0

renderProductionRules : List String -> Int -> List (Html Msg)
renderProductionRules rules index =
  let
    renderButton : Int -> Bool -> String -> (Html Msg)
    renderButton v marked txt =
      if marked then
        button [class "uk-button uk-button-primary", buttonStyle, onClick (ProductionNumber v)] [text txt]
      else
        button [class "uk-button uk-button-default", buttonStyle, onClick (ProductionNumber v)] [text txt]

    generateButtons lst i =
      case lst of
        [] -> []
        (x :: xs) -> 
          if index == i then
            (renderButton i True x) :: (generateButtons xs (i + 1))
          else
            (renderButton i False x) :: (generateButtons xs (i + 1))
  in
    generateButtons rules 0

productions : List String 
-- productions = ["S -> D e", "D -> C d", "C -> B c", "B -> A b", "A -> a"]
productions = (List.map CFG.toString (Array.toList grammar.productions))

-- view model =
--   case model of
--     State s -> text (String.join " " (Array.toList s))
view : Model -> Html Msg
view model =
  div []
    [ p [class "uk-margin"] [text "Select the position in the string where you would like to apply a rule:"]
    , div [class "uk-button-group uk-flex-wrap"] (renderStringCharacter (Array.toList model.state) (case model.index of 
                                                    Just p -> p
                                                    Nothing -> -1))

    , p [class "uk-margin"] [text "Select the production which you wish to apply at the selected position:"]
    , div [class "uk-button-group uk-flex-wrap"] (renderProductionRules productions (case model.production_number of 
                                                    Just i -> i
                                                    Nothing -> -1))

    , div [class "uk-margin"] [button [ class "uk-button uk-button-primary", onClick Transition ] [ text "Reduce" ]]
    ]